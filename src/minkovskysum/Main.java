/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package minkovskysum;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Locale;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * @author Artem
 */
public class Main {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI();
            }
        });
    }
    
    private static void createAndShowGUI() {
        JFrame f = new JFrame("Minkovsky Sum Demo");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(800,600);
        f.setResizable(false);
        f.setLocation(100, 100);
        f.setVisible(true);
        f.add(new MyPanel());
        f.pack();
    }
}

class MyPanel extends JPanel {
    
    Polygon polygon1;
    Polygon polygon2;
    Polygon minkovskySum;
    Polygon sum;
    Point mousePositionTransformed;
    Point mousePosition;
    
    AffineTransform tx = new AffineTransform();

    public MyPanel() {
        //setBorder(BorderFactory.createLineBorder(Color.black));
        addMouseWheelListener(new ZoomHandler());
        addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent e)
            {
                mousePosition = new Point(e.getX(), e.getY());
                try {
                    AffineTransform inverse = tx.createInverse();
                    Point p = Point.getTransformedPoint(inverse, new Point(e.getX(), e.getY()));
                    mousePositionTransformed = new Point(p.X, getHeight() - p.Y);
                    repaint();
                } catch (NoninvertibleTransformException ex) {
                    Logger.getLogger(MyPanel.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        });
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1)
                {
                    if (polygon1 == null) {
                        polygon1 = new Polygon();
                    }
                    try {
                        ArrayList<Point> points = polygon1.getPoints();
                        AffineTransform inversedTransform = tx.createInverse();
                        Point pointToAdd = Point.getTransformedPoint(inversedTransform, new Point(e.getX(), e.getY()));
                        pointToAdd.Y = getHeight() - pointToAdd.Y;
                        points.add(pointToAdd);
                    } catch (NoninvertibleTransformException ex) {
                        Logger.getLogger(MyPanel.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
                else if (e.getButton() == MouseEvent.BUTTON3)
                {
                    if (polygon2 == null) {
                        polygon2 = new Polygon();
                    }
                    try {
                        ArrayList<Point> points = polygon2.getPoints();
                        AffineTransform inversedTransform = tx.createInverse();
                        Point pointToAdd = Point.getTransformedPoint(inversedTransform, new Point(e.getX(), e.getY()));
                        pointToAdd.Y = getHeight() - pointToAdd.Y;
                        points.add(pointToAdd);
                    } catch (NoninvertibleTransformException ex) {
                        Logger.getLogger(MyPanel.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                else if (e.getButton() == MouseEvent.BUTTON2)
                {
                    polygon1 = null;
                    polygon2 = null;
                }
                minkovskySum = Polygon.getMinkovskySum(polygon1, polygon2);
                sum = Polygon.getSimpleSum(polygon1, polygon2);
                repaint();
            }
        });
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(800, 600);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);       
        drawAxes(g);
        drawPolygon(polygon1, Color.BLUE, g);
        drawPolygon(polygon2, Color.GREEN, g);
        drawPolygon(minkovskySum, Color.RED, g);
        if (sum != null)
        {
            drawPolygonPoints(sum, g, Color.ORANGE);
        }
        drawMouseCoordinates(g);
        drawTip(g);
    }  
    
    public void drawAxes(Graphics g) {
        g.setColor(Color.BLACK);
        Point origin = Point.getTransformedPoint(tx, new Point(0,getHeight()));
        g.drawLine(origin.X, 0, origin.X, getHeight());
        g.drawLine(0, origin.Y, getWidth(), origin.Y);
    }
    
    public void drawMouseCoordinates(Graphics g) {
        if (mousePositionTransformed != null && mousePosition != null) {
            drawPointCoordinates(mousePositionTransformed, mousePosition, g, Color.BLACK);
        }
    }
    
    public void drawPointCoordinates(Point p, Point position, Graphics g, Color c) {
        g.setColor(c);
        g.drawString(String.format("%s, %s", 
                                        Integer.toString(p.X), 
                                        Integer.toString(p.Y)),
                     position.X, position.Y); 
    }
    
    public void drawTip(Graphics g)
    {
        g.setColor(Color.RED);
        String mes1 = "";
        String mes2 = "";
        if (polygon1 != null)
        {
            mes1 = polygon1.IsConvex() == false ? "Polygon 1 is not convex!\n" : "";
        }
        if (polygon2 != null)
        {
            mes2 = polygon2.IsConvex() == false ? "Polygon 2 is not convex!" : "";
        }
        g.drawString(String.format("%s\n %s", mes1, mes2),
                     0, getHeight()); 
    }
    
    private void drawPolygon(Polygon polygon, Color color, Graphics g)
    {
        g.setColor(color);
        if (polygon != null) {
            ArrayList<Point> pointsToDraw;
            
            ArrayList<Point> polygonPoints = polygon.getPoints();
            pointsToDraw = new ArrayList<>(polygonPoints.size());
            
            for (int i = 0; i < polygonPoints.size(); i++)
            {
                Point point = polygonPoints.get(i);
                point = new Point(point.X, getHeight() - point.Y);
                pointsToDraw.add(Point.getTransformedPoint(tx, point));
            }
            
            for (Point point : pointsToDraw) {
                g.fillRect(point.X - 2, point.Y - 2, 5, 5);
            }
            
            //Highlight first point (for debug purposes)
            g.setColor(Color.YELLOW);
            Point point = pointsToDraw.get(0);
            g.fillRect(point.X - 2, point.Y - 2, 5, 5);
            
            g.setColor(color);
            for (int i = 1; i <= polygonPoints.size(); i++)
            {
                Point start = pointsToDraw.get(i - 1);
                int endPointIndex = i == pointsToDraw.size() ? 0 : i;
                Point end = pointsToDraw.get(endPointIndex);
                g.drawLine(start.X, start.Y, end.X, end.Y);
                drawPointCoordinates(polygonPoints.get(endPointIndex), end, g, color);
            }
        }
    }
    
    private void drawPolygonPoints(Polygon polygon, Graphics g, Color color)
    {
        g.setColor(color);
        if (polygon != null) {
            ArrayList<Point> points = polygon.getPoints();
            ArrayList<Point> pointsToDraw;
            pointsToDraw = new ArrayList<>(points.size());
            
            
            for (int i = 0; i < points.size(); i++)
            {
                Point point = points.get(i);
                point = new Point(point.X, getHeight() - point.Y);
                pointsToDraw.add(Point.getTransformedPoint(tx, point));
            }
            
            for (Point point : pointsToDraw) {
                g.fillRect(point.X - 2, point.Y - 2, 5, 5);
            }
        }
    }
    
    private class ZoomHandler implements MouseWheelListener {
        double scale = 1.0;

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            if (e.getScrollType() == MouseWheelEvent.WHEEL_UNIT_SCROLL) {
                Point2D p1 = e.getPoint();
                Point2D p2;
                try {
                    p2 = tx.inverseTransform(p1, null);
                } catch (NoninvertibleTransformException ex) {
                    // should not get here
                    ex.printStackTrace();
                    return;
                }

                scale -= (0.1 * e.getWheelRotation());
                scale = Math.max(0.1, scale);

                tx.setToIdentity();
                tx.translate(p1.getX(), p1.getY());
                tx.scale(scale, scale);
                tx.translate(-p2.getX(), -p2.getY());

                MyPanel.this.revalidate();
                MyPanel.this.repaint();
            }
	}
    }
}

