/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package minkovskysum;

/**
 *
 * @author Artem
 */
public class Vector {
    public Point start;
    public Point end;
      
    public Vector(Point start, Point end)
    {
        this.start = start;
        this.end = end;
    }
    
    public int getHeight()
    {
        return end.Y - start.Y;
    }
    
    public int getWidth()
    {
        return end.X - start.X;
    }
}
