/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package minkovskysum;

import java.awt.geom.AffineTransform;

/**
 *
 * @author Artem
 */
public class Point {
    public int X;
    public int Y;
    
    public Point(int x, int y)
    {
        X = x;
        Y = y;
    }
    
    public Point()
    {
        X = 0;
        Y = 0;            
    }
    
    public static Point getSum(Point a, Point b)
    {
        return new Point(a.X + b.X, a.Y + b.Y);
    }
    
    public static Point getTransformedPoint(AffineTransform at, Point p)
    {
        Point result = new Point();
        
        result.X = (int)(p.X * at.getScaleX() + p.X * at.getShearX() + at.getTranslateX());
        result.Y = (int)(p.X * at.getShearY() + p.Y * at.getScaleY() + at.getTranslateY());
        
        return result;
    }
}
