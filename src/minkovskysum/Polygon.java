/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package minkovskysum;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Artem
 */
public class Polygon {
    
    private ArrayList<Point> Points;
    
    public Polygon()
    {
        Points = new ArrayList<>();
    }
    
    public Polygon(ArrayList<Point> p)
    {
        Points = p;
    }
    
    public Boolean IsConvex()
    {
        Point firstPoint;
        Point secondPoint;
        Point thirdPoint;
    
        Boolean wedgePositive = null;
        
        int size = Points.size();
        
        for (int i = 2; i < size + 2; i++)
        {
            int k;
            k = i - 2;
            firstPoint = Points.get(k);
            k = (i - 1) % size;
            secondPoint = Points.get(k);
            k = i % size;
            thirdPoint = Points.get(k);
            
            int wedge = Geometry.GetWedgeProduct(firstPoint, secondPoint, thirdPoint);
            if (wedgePositive == null) 
            {
                wedgePositive = wedge >= 0;
            }
            else
            {
                if (wedge >= 0 != wedgePositive) {
                    return false;
                }
            }
        }
        return true;
    }
    
    private void defineStartPoint()
    {
        int startPointIndex = 0;
        for (int i = 0; i < Points.size(); i++)
        {
            Point start = Points.get(startPointIndex);
            Point current = Points.get(i);
            if (current.Y <= start.Y) {
                startPointIndex = i;
            }
        }
        ArrayList<Point> newArray = new ArrayList<>();
        newArray.addAll(Points.subList(startPointIndex, Points.size()));
        newArray.addAll(Points.subList(0, startPointIndex));
        
        Points = newArray;
    }
    
    public static Polygon getMinkovskySum(Polygon a1, Polygon b1)
    { 
        if (a1 == null || b1 == null) {
            return null;
        }
        
        Polygon a = a1.getPreProcessed();
        Polygon b = b1.getPreProcessed();
        
        if (a == null || b == null) {
            return null;
        }
        
        int aSize = a.getPoints().size();
        int bSize = b.getPoints().size();
        
        int i = 0;
        int j = 0;
        Polygon result = new Polygon(new ArrayList<Point>());
        while (i != aSize + 1 &&
               j != bSize + 1)
        {
            
            result.Points.add(Point.getSum(a.Points.get(i % aSize), b.Points.get(j % bSize)));
            int wedgeProduct = Geometry.GetWedgeProduct(a.Points.get(i % aSize), 
                                                        a.Points.get((i + 1) % aSize), 
                                                        b.Points.get(j % bSize), 
                                                        b.Points.get((j + 1) % bSize));
            if (wedgeProduct > 0)
            {
                i++;
            }
            else if (wedgeProduct < 0)
            {
                j++;
            }
            else
            {
                i++;
                j++;
            }
        }
        if (i < aSize - 1)
        {
            for (; i < aSize; i++)
            {
                result.Points.add(Point.getSum(a.Points.get(i), b.Points.get(0)));
            }
        }
        if (j < bSize - 1)
        {
            for (; j < bSize; j++)
            {
                result.Points.add(Point.getSum(a.Points.get(0), b.Points.get(j)));
            }
        }
        
        return result;
    }
    
    public static Polygon getSimpleSum(Polygon a, Polygon b)
    { 
        if (a == null || b == null) {
            return null;
        }
        if (!a.IsConvex() || !b.IsConvex()) {
            return null;
        }
        
        Polygon result = new Polygon();
        for (int i = 0; i < a.Points.size(); i++) {
            for (int j = 0; j < b.Points.size(); j++) {
                result.Points.add(new Point(a.Points.get(i).X + b.Points.get(j).X, 
                                            a.Points.get(i).Y + b.Points.get(j).Y));
            }
        }
        return result;
    }
    
    private Polygon getPreProcessed()
    {
        int size = Points.size();
        
        if (!IsConvex() || size < 3) {
            return null;
        }
        Polygon result = new Polygon();
        result.Points = (ArrayList<Point>)Points.clone();
        if (Geometry.GetWedgeProduct(Points.get(0), 
                                     Points.get(1 % size), 
                                     Points.get(2 % size)) < 0)
        {
            Collections.reverse(result.Points);
        }
        
        result.defineStartPoint();
        
        return result;
    }
    
    public ArrayList<Point> getPoints()
    {
        return Points;
    }
    
    public void PrintInfo()
    {
        System.out.println("************POLYGON**************");
        System.out.println("Points:");
        for (int i = 0; i < Points.size(); i++)
        {
            System.out.println("(" + Points.get(i).X + ", " + Points.get(i).Y + ")");
        }
        System.out.println("Polygon is " + (IsConvex() ? "" : "not ") + "convex");
    }
}
