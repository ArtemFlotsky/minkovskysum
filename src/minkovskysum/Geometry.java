/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package minkovskysum;

/**
 *
 * @author Artem
 */
public class Geometry {
    
    private Geometry(){};
    
    public static int GetWedgeProduct(Vector a, Vector b)
    {
        return a.getWidth() * b.getHeight() - a.getHeight() * b.getWidth();
    }
    
    public static int GetWedgeProduct(Point a, Point b, Point c)
    {
        Vector v1 = new Vector(a, b);
        Vector v2 = new Vector(b, c);
        
        return GetWedgeProduct(v1, v2);
    }  
    
    public static int GetWedgeProduct(Point v1Start, Point v1End, Point v2Start, Point v2End)
    {
        int heightDiff = v1End.Y - v2Start.Y;
        int widthDiff = v1End.X - v2Start.X;
        Point p = new Point(v2End.X + widthDiff, v2End.Y + heightDiff);
        return GetWedgeProduct(v1Start, v1End, p);
    }
}
